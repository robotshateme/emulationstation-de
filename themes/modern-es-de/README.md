# Modern for ES-DE Frontend (modern-es-de)

The following options are included:

4 variants:

- Textlist with videos
- Textlist without videos
- Textlist with videos (legacy)
- Textlist without videos (legacy)

2 color schemes:

- Dark
- Light

2 font sizes:

- Medium
- Large

4 aspect ratios:

- 16:9
- 16:10
- 4:3
- 21:9

8 languages:

- English (United States)
- English (United Kingdom)
- Español (España)
- Français
- Italiano
- Português (Brasil)
- Română
- Svenska

3 transitions:

- Instant
- Instant and slide
- Instant and fade

Credits and license information can be found next to this file.
