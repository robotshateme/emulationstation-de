# Linear for ES-DE Frontend (linear-es-de)

The following options are included:

6 variants:

- Textlist
- Textlist without videos
- Simple textlist
- Simple textlist without videos
- Carousel
- Simple carousel

5 color schemes:

- Dark
- Light
- NSO
- LIVE
- PSN

2 font sizes:

- Medium
- Large

4 aspect ratios:

- 16:9
- 16:10
- 4:3
- 21:9

8 languages:

- English (United States)
- English (United Kingdom)
- Español (España)
- Français
- Italiano
- Português (Brasil)
- Română
- Svenska

3 transitions:

- Instant and slide
- Instant
- Fade

Credits and license information can be found next to this file.
